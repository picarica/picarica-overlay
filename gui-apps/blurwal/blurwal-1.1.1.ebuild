# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_6 )

inherit distutils-r1 eapi7-ver gnome2-utils python-r1 toolchain-funcs python-utils-r1

DESCRIPTION="A background application that smoothly blurs the wallpaper when windows are opened."
HOMEPAGE="https://gitlab.com/BVollmerhaus/blurwal "
SRC_URI="https://gitlab.com/BVollmerhaus/${PN}/-/archive/${PV}/${P}.tar.gz -> ${P}.tar.gz"


LICENSE="MIT License"
SLOT="0"
KEYWORDS="*"
IUSE="feh"

DEPEND="
	dev-python/python-xlib[${PYTHON_USEDEP}]
	dev-python/pytest-runner[${PYTHON_USEDEP}]
	>=dev-lang/python-3.6
	media-gfx/imagemagick
	feh? ( media-gfx/feh )
	!feh? ( xfce-base/xfconf )
"

RDEPEND="${DEPEND}"


pkg_postinst() {
	einfo "if auto-detecting backedn doesnt work set it manually:"
	einfo "blurwal --backend feh/xfce"
	einfo "for mroe info type blurwal --help"
}
