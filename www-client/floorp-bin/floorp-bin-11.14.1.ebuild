# Copyright 2020-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit desktop linux-info optfeature pax-utils xdg

MY_PN="Cura-${PV}.Appimage"

DESCRIPTION="A Browser built for keeping the Open Web alive. Based on Mozilla Firefox."
HOMEPAGE="https://floorp.app/en"
SRC_URI="https://github.com/Floorp-Projects/Floorp/releases/download/v${PV}/floorp-${PV}.linux-x86_64.tar.bz2 -> ${PN}.tar.bz2"

LICENSE="MPL-2.0 GPL-2 LGPL-2.1"
SLOT="0"
KEYWORDS="amd64"

RESTRICT="strip"

IUSE="+alsa +ffmpeg +gmp-autoupdate +pulseaudio selinux wayland"

BDEPEND="app-arch/unzip"
RDEPEND="${DEPEND}
	!www-client/firefox-bin:0
	!www-client/firefox-bin:esr
	>=app-accessibility/at-spi2-core-2.46.0:2
	>=dev-libs/glib-2.26:2
	media-libs/alsa-lib
	media-libs/fontconfig
	>=media-libs/freetype-2.4.10
	sys-apps/dbus
	virtual/freedesktop-icon-theme
	>=x11-libs/cairo-1.10[X]
	x11-libs/gdk-pixbuf:2
	>=x11-libs/gtk+-3.11:3[X,wayland?]
	x11-libs/libX11
	x11-libs/libXcomposite
	x11-libs/libXcursor
	x11-libs/libXdamage
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXi
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libxcb
	>=x11-libs/pango-1.22.0
	alsa? (
		!pulseaudio? ( media-sound/apulse )
	)
	ffmpeg? ( media-video/ffmpeg )
	pulseaudio? ( media-libs/libpulse )
	selinux? ( sec-policy/selinux-mozilla )
"
RDEPEND="

"

RESTRICT="binchecks strip"

S=${WORKDIR}

src_install() {
	insinto "/opt/floorp"
	unpack "${PN}.tar.bz2"
	newins "floorp" "floorp"
}

pkg_postinst() {
	elog "hope this works"
	elog "Testing veriosn"
}


